class CreateHogeItem < ActiveRecord::Migration[5.2]
  def up
    create_table :hoge_items do |t|
      t.string :name, null: false, index: { unique: true }
      t.datetime :created_at, null: false
    end
  end

  def down
    drop_table :hoge_items
  end
end