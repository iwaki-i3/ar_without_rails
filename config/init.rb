require 'active_record'
require 'active_support'
require 'active_support/all'
require 'bundler/setup'

config_dir = File.dirname(__FILE__)

# データベース接続の定義
Time.zone = 'Tokyo'
database_config = YAML::load(ERB.new(File.read(File.join(config_dir, 'database.yml'))).result)
ActiveRecord::Base.establish_connection(database_config['development'])
ActiveRecord::Base.time_zone_aware_attributes = true

# モデルのクラスをrequireしておく
model_files = File.join(config_dir, '../models', '**', '*.rb')
Dir.glob(model_files).map{|f| require f}
